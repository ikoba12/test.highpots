package ge.highpots.service;

import ge.highpots.exception.ConversionException;
import ge.highpots.model.Product;

/**
 * This class is generic in order to return the data that we want, in the assignment the desired data should have been presented in JSON format
 * but if we want to change it to XML we just implement this interface with any type we like
 *
 */
public interface ProductConverterService<T> {

    T convert(Product product)  throws ConversionException;
}
