package ge.highpots.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "products")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductListBeanXml {

    @XmlElement(name = "product")
    private List<ProductBeanXml> productBeanList;

    public ProductListBeanXml() {
    }

    public ProductListBeanXml(List<ProductBeanXml> productBeanList) {
        this.productBeanList = productBeanList;
    }

    public List<ProductBeanXml> getProductBeanList() {
        return productBeanList;
    }

    public void setProductBeanList(List<ProductBeanXml> productBeanList) {
        this.productBeanList = productBeanList;
    }

    @Override
    public String toString() {
        return "ProductListBeanXml{" +
                "productBeanList=" + productBeanList +
                '}';
    }
}
