import ge.highpots.service.XmlParserService
import spock.lang.Specification

class XmlParserServiceSpec extends Specification{

    def FILE_WITH_2_PRODUCTS = "testProducts.xml"
    def xmlParser
    def setup(){
        xmlParser = new XmlParserService();
    }

    def "should return valid product list"(){
        when: "trying to read valid bean file with 2 products"
        def result = xmlParser.parseProductList(FILE_WITH_2_PRODUCTS)

        then : "result should be present"
        result.isPresent()

        and: "result list should have size 2"
        def list = result.get().getProductBeanList()
        list.size() == 2

        and: "products should have correct values"
        def p1 = list.get(0);
        def p2 = list.get(1);
        p1.name == "test1"
        p2.name == "test2"
        p1.price.compareTo(BigDecimal.valueOf(15.60 as double)) == 0
        p2.price.compareTo(BigDecimal.valueOf(18.60 as double)) == 0
        p1.description == "testDesc1"
        p2.description == "testDesc2"
    }

}
