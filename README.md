# test.highpots

Demo Application for Highpots

Running the application:

Prerequisites:

Java 8 is installed

port 8080 is free to use

Command line running:

    Prerequisites: gradle is installed and can be used from command line
    
    1. Clone the project
    2. Open cmd, go to project base directory
    3. run 'gradlew build'
    4. cd to build/libs
    5. run 'java -jar test-1.0-SNAPSHOT.war'

Intellij running :
    
    1. Navigate to : File->New->Project From Version Control -> Git
    2. enter git repo address and clone
    3. run AppLauncher.Java
    
Eclipse running :
    You are on your own my friend....
    
NetBeans running :
    Why are people using Netbeans???