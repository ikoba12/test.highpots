package ge.highpots.service;

import ge.highpots.bean.BaseProductBean;
import ge.highpots.bean.ProductBeanJson;
import ge.highpots.exception.ConversionException;
import ge.highpots.model.Product;
import ge.highpots.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductServiceImpl implements ProductService{

    private static Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    private ProductRepository productRepository;

    private ProductConverterService<ProductBeanJson> productConverterService;

    public ProductServiceImpl(ProductRepository productRepository, ProductConverterService<ProductBeanJson> productConverterService) {
        this.productRepository = productRepository;
        this.productConverterService = productConverterService;
    }

    @Override
    public List<BaseProductBean> getProductList() throws RuntimeException{
        logger.debug("[IN] getProductList");
        List<BaseProductBean> result = new ArrayList<>();
        try{
            Iterable<Product> products = productRepository.findAll();
            products.forEach(product -> {
                try {
                    result.add(productConverterService.convert(product));//My understanding here is that if some(or even all) of the products failed to load we should still return the result and not error out
                } catch (ConversionException e) {
                    logger.error("Error while convert", e);
                }
            });
        }catch (Exception e){
            logger.error("Error while retrieving/converting data", e);
            throw new RuntimeException(e.getMessage());
        }
        logger.debug("[OUT] getProductList");
        return result;
    }
}
