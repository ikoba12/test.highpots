package ge.highpots.service;

import ge.highpots.repository.ProductRepository;
import ge.highpots.bean.ProductListBeanXml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class DataInitializerService {

    private static Logger logger = LoggerFactory.getLogger(DataInitializerService.class);


    private final String productsFileName;

    private ProductRepository productRepository;

    private XmlParserService xmlParserService;

    @Autowired
    public DataInitializerService(@Value("${products.file.name}") String productsFileName, ProductRepository productRepository, XmlParserService xmlParserService) {
        this.productsFileName = productsFileName;
        this.productRepository = productRepository;
        this.xmlParserService = xmlParserService;
    }

    /*
        From my understanding if the data could not be loaded the application
        should not launch so there will be an exception throw in this case
     */
    @PostConstruct
    public void init(){
        logger.debug("[IN] init");
        logger.info(String.format("Attempting to read file %s",productsFileName));
        Optional<ProductListBeanXml> productListBean = xmlParserService.parseProductList(productsFileName);
        productListBean.orElseThrow(() -> new RuntimeException("Data not available!!!")).getProductBeanList().forEach(productBean -> {
            try {
                productRepository.save(productBean.toProduct());
            } catch (Exception e) {
                logger.error("Error while saving product", e);
            }
        });
        logger.debug("[OUT] init");
    }
}
