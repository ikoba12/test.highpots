package ge.highpots.service;

import ge.highpots.bean.BaseProductBean;

import java.util.List;

public interface ProductService {

    List<BaseProductBean> getProductList() throws RuntimeException;
}
