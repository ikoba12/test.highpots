package ge.highpots.service;

import ge.highpots.bean.ProductBeanJson;
import ge.highpots.exception.ConversionException;
import ge.highpots.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ProductConverterServiceJsonImpl implements ProductConverterService<ProductBeanJson> {

    private static Logger logger = LoggerFactory.getLogger(ProductConverterServiceJsonImpl.class);


    @Override
    public ProductBeanJson convert(Product product) throws ConversionException{
        logger.debug("[IN] convert");
        ProductBeanJson productBeanJson = new ProductBeanJson();
        try{
            productBeanJson.setId(product.getId());
            productBeanJson.setDescription(product.getDescription());
            productBeanJson.setName(product.getName());
            productBeanJson.setPrice(product.getPrice());

        }catch (Exception e){
            logger.error("Error while converting product to json product bean" ,e);
            throw new ConversionException("Error while converting product");
        }
        logger.debug("[OUT] convert");
        return productBeanJson;
    }
}
