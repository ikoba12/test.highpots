package ge.highpots.exception;

public class ConversionException extends Exception {
    public ConversionException(String message) {
        super(message);
    }
}
