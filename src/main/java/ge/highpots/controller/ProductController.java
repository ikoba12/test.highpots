package ge.highpots.controller;

import ge.highpots.service.ProductConverterServiceJsonImpl;
import ge.highpots.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductController {

    private ProductService productService;

    private static Logger logger = LoggerFactory.getLogger(ProductConverterServiceJsonImpl.class);

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity getProducts(){
        logger.debug("[IN] getProducts");
        ResponseEntity responseEntity;
        try {
            responseEntity = new ResponseEntity(productService.getProductList(), HttpStatus.OK);
        }catch (Exception e){
            responseEntity = new ResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.debug("[OUT] getProducts");
        return responseEntity;
    }
}
