package ge.highpots.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * Interceptor for inbound request and outbound response. Would be a good idea to also log request(if body present)/ response bodies.
 * For now just logging endpoint, method and response status
 */
public class LoggingInterceptor extends HandlerInterceptorAdapter {
    private static Logger logger = LoggerFactory.getLogger(LoggingInterceptor.class);


    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) {

        logger.info("[preHandle]" + "[" + request.getMethod()
                + "]" + request.getRequestURI());

        return true;
    }

    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response,Object handler, Exception ex) {
        if (ex != null){
            ex.printStackTrace();
        }
        logger.info("[afterCompletion][" + response.getStatus() + "][exception: " + ex + "]");
    }


}
