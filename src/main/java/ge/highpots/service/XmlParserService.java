package ge.highpots.service;

import ge.highpots.bean.ProductListBeanXml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.Optional;

@Service
public class XmlParserService {

    private static Logger logger = LoggerFactory.getLogger(XmlParserService.class);

    public Optional<ProductListBeanXml> parseProductList(String fileName) {
        logger.debug("[IN] parseProductList");
        logger.debug(String.format("Parsing product List from file with filename %s", fileName));
        Optional<ProductListBeanXml> productListBean = Optional.empty();
        try {
            InputStream file = getClass().getResourceAsStream(String.format("/%s",fileName));
            JAXBContext jaxbContext = JAXBContext.newInstance(ProductListBeanXml.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            productListBean = Optional.of((ProductListBeanXml) unmarshaller.unmarshal(file));

        }catch (JAXBException e){
            logger.error("Error while unmarshalling", e);
        }catch (Exception e){
            logger.error("Error while trying to parse file", e);
        }
        logger.debug(String.format("Result of file parsing %s", productListBean));
        logger.debug("[OUT] parseProductList");
        return productListBean;
    }
}
