package ge.highpots.bean;

import ge.highpots.model.Product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ProductBeanXml extends BaseProductBean{

    @XmlElement(name = "name")
    public void setName(String name) {
        super.setName(name);
    }

    @XmlElement(name = "description")
    public void setDescription(String description) {
        super.setDescription(description);
    }

    @XmlElement(name = "price")
    public void setPrice(BigDecimal price) {
        super.setPrice(price);
    }

    public Product toProduct(){
        Product product = new Product(getName(),getDescription(),getPrice());
        return product;
    }

}
